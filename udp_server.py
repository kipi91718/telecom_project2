import socket
from io import StringIO


#UDP_IP = "140.114.85.135" #need to modify to get from input?
#UDP_PORT = 55688
UDP_IP = input("Your IP : ")
str_port = input("Your port : ")
UDP_PORT = int(str_port)
try:
	sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM) # UDP socket
	sock.bind((UDP_IP, UDP_PORT))
except Exception as e:
	print("Error in socket binding")

print("Start to listen to the SIP request")

user_dict = dict()

while True:
    data, client_addr = sock.recvfrom(4096) # buffer size is 1024 bytes
    in_str = StringIO(data.decode("ASCII"))	# SIP uses ASCII code to transfer
    #identify the type of SIP packet
    first_line = in_str.readline()

    if first_line.startswith("ACK"):
    	continue

    elif first_line.startswith("REGISTER"):
    	#mode = "REGISTER"

    	resp_msg = "SIP/2.0 200 OK\r\n"
    	To_str = ""
    	Contact_str = ""
    	while True:
    		line_str = in_str.readline()
    		if line_str == "":	#end of packet, sent the response message and break
    			user_dict[To_str] = Contact_str
    			#print(user_dict)
    			sock.sendto(bytes(resp_msg, "ASCII"), client_addr)
    			break
    		if line_str.startswith("Max-Forwards"):	#Don't need to send back
    			continue
    		elif line_str.startswith("To"):
    			#get the "To" info
    			To_start = (line_str.index("sip:")) + 4
    			To_end = line_str.index('@')
    			To_str = line_str[To_start:To_end]

    		elif line_str.startswith("Contact"):
    			#get the "Contact" info
    			Contact_start = (line_str.index('@')) + 1
    			Contact_end = line_str.index('\r')
    			Contact_str = line_str[Contact_start:Contact_end]

    		resp_msg = resp_msg + line_str


    elif first_line.startswith("INVITE"):
    	#get the destination wanted first
    	dest_start = first_line.index("sip:")+4
    	dest_end = first_line.index('@')
    	dest_str = first_line[dest_start:dest_end]
    	#print("dest_str = " + dest_str)
    	resp_msg = "SIP/2.0 302 Moved Temporarily\r\n"
    	str_contact = "Contact:sip:"
    	str_contact = str_contact + dest_str
    	str_contact = str_contact + "@"
    	str_contact = str_contact + user_dict[dest_str]
    	str_contact = str_contact + "\r\n"
    	#print(str_contact)
    	resp_msg = resp_msg + str_contact;
    	while True:
    		line_str = in_str.readline()

    		if line_str == "":

    			#resp_msg = resp_msg + str_contact
    			sock.sendto( bytes(resp_msg, "ASCII"), client_addr )
    			break

    		elif line_str.startswith("Contact"):
    			continue
    		else:
    			resp_msg = resp_msg + line_str
    			continue

